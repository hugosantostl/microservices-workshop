package com.sensedia.notification.applications;

import com.sensedia.notification.adapters.dtos.NotificationDto;
import com.sensedia.notification.adapters.http.SlackClient;
import com.sensedia.notification.domains.Notification;
import com.sensedia.notification.ports.ApplicationPort;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Service
@Transactional
@Validated
@Slf4j
public class NotificationApplication implements ApplicationPort {

    private final SlackClient slackClient;

    @Autowired
    public NotificationApplication(SlackClient slackClient) {
        this.slackClient = slackClient;
    }

    @Override
    public void notify(@Valid @NotNull Notification notification) {
        NotificationDto dto = new NotificationDto();
        dto.setText(notification.getText());
        this.slackClient.publishMessage(dto);
    }
}
